<?php declare(strict_types=1);

namespace Plugin\jtl_append_consent;

use JTL\Events\Dispatcher;
use JTL\Plugin\Bootstrapper;

/**
 * Class Bootstrap
 * @package Plugin\jtl_append_consent
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @inheritdoc
     */
    public function boot(Dispatcher $dispatcher)
    {
        parent::boot($dispatcher);
    }
}
