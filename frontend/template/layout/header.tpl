{block name='layout-header-head-resources' append}
    <script>
        document.addEventListener('consent.ready', function(e) {
            myConsentFunction(e.detail);
        });
        document.addEventListener('consent.updated', function(e) {
            myConsentFunction(e.detail);
        });
        function myConsentFunction(detail) {
            if (detail !== null && typeof detail.myuniqueid !== 'undefined') {
                if (detail.myuniqueid === true) {
                    console.log('myuniqueid has consent!');
                } else {
                    console.log('myuniqueid has NO consent.');
                }
            }
        }
    </script>
{/block}
